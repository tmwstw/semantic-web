<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <div class="toc-contents">
            <ul>
                <xsl:apply-templates/>
            </ul>
        </div>
    </xsl:template>
    <xsl:template match="item">
        <li>
            <div class="toc-item">
                <a>
                    <xsl:attribute name="href">
                        https://www.ebay.com/b/Vehicle-Electronics-GPS/3270/bn_887004<xsl:value-of select="link"/>
                    </xsl:attribute>
                    <h1><xsl:value-of select="title"/></h1>
                </a>
                <p>price: <xsl:value-of select="price"/></p>
                <p>shipping: <xsl:value-of select="shipping"/></p>
                <p>Answers: <xsl:value-of select="answers"/></p>
                <a>
                <xsl:attribute name="href">
                    https://www.ebay.com/b/Vehicle-Electronics-GPS/3270/bn_887004<xsl:value-of select="url"/>
                </xsl:attribute>
                <p>Image link<xsl:value-of select="image_src"/></p>
                </a>
                <p>offer: <xsl:value-of select="offer"/></p>
                <p>location: <xsl:value-of select="location"/></p>
                <p>hotness: <xsl:value-of select="hotness"/></p>
            </div>
        </li>
    </xsl:template>
</xsl:stylesheet>