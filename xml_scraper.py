"""
Python web-scraper to retrieve xml data
"""

import re
import requests
import xmlschema

from bs4 import BeautifulSoup
from dicttoxml import dicttoxml

URL = "https://www.ebay.com/b/Vehicle-Electronics-GPS/3270/bn_887004"
ATTRIBUTE = "l1s-container"
XML = "initial.xml"
XSD = "initial.xsd"


def get_products():
    page = requests.get(URL)

    soup = BeautifulSoup(page.content, "lxml")

    products_html = soup.findAll("li", {"class": lambda key: key and key.startswith("s-item")})

    products = []

    for pid, html in enumerate(products_html):
        products.append({})

        products[pid]["title"] = str(html.find("h3", {"class", "s-item__title"}).contents[0])
        products[pid]["link"] = str(html.find("a", {"class", "s-item__link"})["href"])
        products[pid]["price"] = float(html.find("span", {"class", "s-item__price"}).contents[0][1:])
        try:
            products[pid]["shipping"] = str(
                html.find("span", {"class": lambda key: key and key.startswith("s-item__shipping")}).contents[0])
        except AttributeError:
            products[pid]["shipping"] = ""
        products[pid]["image_src"] = str(html.find("span", {"class", "s-item__price"}))
        try:
            products[pid]["offer"] = str(
                html.find("span", {"class": lambda key: key and key.startswith("s-item__purchase-options")}).contents[
                    0])
        except AttributeError:
            products[pid]["offer"] = ""
        try:
            products[pid]["location"] = str(
                html.find("span", {"class": re.compile("s-item__itemLocation")}).contents[0])
        except AttributeError:
            products[pid]["location"] = ""
        try:
            products[pid]["hotness"] = str(
                html.find("span", {"class": lambda key: key and key.startswith("s-item__hotness")}).find(
                    "span").contents[0])
        except AttributeError:
            products[pid]["hotness"] = ""
        try:
            products[pid]["location"] = str(
                html.find("span", {"class": re.compile("s-item__bidCount")}).contents[0])
        except AttributeError:
            products[pid]["location"] = ""

    return products


if __name__ == "__main__":

    with open(XML, "wb") as xml:
        xml.write(dicttoxml(get_products()))

    # flag = bool(input("Do you have XSD? "))
    # if flag:
    #     schema = xmlschema.XMLSchema(XSD)
    #     print(schema.is_valid(XML))
    # else:
    #     pass
