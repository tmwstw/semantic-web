from rdflib import Graph, Literal, URIRef, BNode
from rdflib.namespace import DC, RDF, RDFS
from xml.etree import ElementTree
from pyshacl import validate
from rdflib import Namespace
import rdflib


SITE_URL = "https://www.ebay.com/b/Vehicle-Electronics-GPS/3270/bn_887004"
XML_FILE = "file.xml"
RDF_FILE = "file.rdf"
FIELDS = {field: index for index, field in enumerate([
    'title', 'link', 'price', 'shipping', 'image_src',
    'offer', 'location', 'hotness',
])}
XML_TO_RDF_MAPPING = {field: field for field in FIELDS}
CLASSES_MAPPING = (
    (
        "items_list", "listOfGoods",
        "Describes the list of goods", None, None, ()
    ),
    (
        "item", "good", "Describes good like the object",
        "items_list", RDF.Bag, ()
    ),
    (
        "item_info", "goodInfo",
        "Describes good info", "item", RDFS.subClassOf,
        (
            ("title", "title", "Describes good title", DC.title),
            ("price", "price", "Describes good price", DC.extend),
        )
    ),
    (
        "item_billing", "itemBilling",
        "Describes good info", "item", RDFS.subClassOf,
        (
            ("shipping", "shipping", "Describes good shipping", DC.extend),
            ("offer", "offer", "Describes good offer", DC.extend),
            ("hotness", "hotness", "Describes good hotness", DC.extend),
        )
    ),
    (
        "item_media", "itemMedia",
        "Describes good media", "item", RDFS.subClassOf,
        (
            (
                "image_src", "image_src",
                "Describes good image src", DC.references
            ),
            ("link", "link", "Describes good link", DC.references),
        )
    ),
)


def create_classes_and_properties(g, mapping):
    items_classes = dict()

    for class_name, class_id, class_comment, \
            parent_class_name, parent_class_relation, class_properties \
            in mapping[::]:

        if parent_class_name is None:
            parent_class = RDFS.Class
        else:
            parent_class = items_classes[parent_class_name]["node"]

        items_classes[class_name] = dict(
            node=BNode(),
            properties=dict(),
            parent_node=parent_class,
            relation=parent_class_relation
        )

        if parent_class_name is None or parent_class_relation is None:
            g.add((
                items_classes[class_name]["node"],
                RDF.type,
                RDFS.Class
            ))
        else:
            g.add((
                items_classes[class_name]["node"],
                RDFS.subClassOf,
                parent_class
            ))
        g.add((items_classes[class_name]["node"], DC.title, Literal(class_id)))
        g.add((
            items_classes[class_name]["node"],
            RDFS.comment,
            Literal(class_comment)
        ))

        for property_name, property_id, property_comment, \
                propery_class_relation in class_properties:
            items_classes[class_name]["properties"][property_name] = dict(
                node=BNode(),
                relation=propery_class_relation
            )

            g.add((
                items_classes[class_name]["properties"][property_name]["node"],
                RDFS.domain,
                items_classes[class_name]["node"]
            ))
            g.add((
                items_classes[class_name]["properties"][property_name]["node"],
                DC.title,
                Literal(property_id)
            ))
            g.add((
                items_classes[class_name]["properties"][property_name]["node"],
                RDFS.comment,
                Literal(property_comment)
            ))
            g.add((
                items_classes[class_name]["properties"][property_name]["node"],
                RDFS.range,
                RDFS.Literal
            ))
            g.add((
                items_classes[class_name]["properties"][property_name]["node"],
                RDF.type,
                RDF.Property
            ))

    return items_classes


def prepare_data(file, mapping):
    data_ = list()

    root = ElementTree.parse(file).getroot()
    for item in root[::]:
        item_ = dict()

        for field, field_index in FIELDS.items():
            item_[mapping[field]] = item[field_index].text

        data_.append(item_)
    return data_


def fill_rdf_graph(g, cap, items_data, mapping):
    list_of_items = URIRef(SITE_URL)
    g.add((list_of_items, RDF.type, cap["items_list"]["node"]))

    ns = rdflib.Namespace(SITE_URL)
    u = rdflib.term.URIRef(SITE_URL)

    items = BNode()
    for item_data in items_data[:2]:
        for class_name, class_item in list(cap.items())[1:]:
            class_node = BNode()
            g.add((
                class_item["node"],
                class_item["relation"],
                class_node
            ))
            g.add((
                class_node,
                RDF.type,
                class_item["node"]
            ))

            if class_name == "item":
                g.add((class_item["node"], class_item["relation"], items))
            else:
                g.add((
                    class_item["node"],
                    class_item["relation"],
                    class_item["parent_node"]
                ))

            for property_name, property_item in \
                    class_item["properties"].items():
                try:
                    getattr(ns, property_name)
                except AttributeError:
                    g.add((u, ns.name, rdflib.Literal(property_name)))
                    blank = rdflib.BNode()
                    g.add((u, ns.annotated_with, blank))

                property_node = BNode()
                g.add((
                    class_node,
                    property_item["relation"],
                    property_node
                ))
                g.add((
                    property_node,
                    RDF.type,
                    property_item["node"]
                ))
                g.add((
                    property_node,
                    getattr(ns, property_name),
                    Literal(item_data[property_name])
                ))

    g.bind("gd", ns)
    g.bind("dc", DC)
    g.bind("rdfs", RDFS)


if __name__ == "__main__":
    graph = Graph()

    classes_and_properties = \
        create_classes_and_properties(graph, CLASSES_MAPPING)
    data = prepare_data(XML_FILE, XML_TO_RDF_MAPPING)
    fill_rdf_graph(graph, classes_and_properties, data, CLASSES_MAPPING)

    with open(RDF_FILE, "wb") as file:
        file.write(graph.serialize())

    print(validate(graph)[2])

    for subject_, predicat_, object_ in graph:
        print(subject_, predicat_, object_)
