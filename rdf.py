import langid

from rdflib import (
    Graph,
    URIRef,
    BNode,
    Literal
)
from rdflib.namespace import (
    DC
)
from xml.etree import ElementTree


SITE_URL = "https://www.ebay.com/b/Vehicle-Electronics-GPS/3270/bn_887004"
XML_FILE_NAME = "initial.xml"
FIELDS = {field: index for index, field in enumerate([
    'title', 'link', 'price', 'shipping', 'image_src',
    'offer', 'location', 'hotness',
])}


def to_RDF(tree, graph):
    root = tree.getroot()
    root_ = URIRef(SITE_URL)

    for index, item in enumerate(root[:4:]):
        item_ = BNode(f"article_{index}")

        for _, field_index in FIELDS.items():
            item_field_ = Literal(item[field_index].text)
            graph.add((item_, Literal(DC.extent), item_field_))
        graph.add((
            root_,
            DC.extent,
            item_
        ))
    graph.add((root_, DC.has_part, Literal("articles")))
    return graph


if __name__ == "__main__":
    graph = Graph()
    tree = ElementTree.parse(XML_FILE_NAME)

    g = to_RDF(tree, graph)
    g.serialize("file.rdf", format="pretty-xml")

    print(f"1. Вивести загальну кількість триплетів: {len(g)}\n")

    print(f"3. Вивести всі триплети \"ресурс-властивість-значення\" \
        (\"суб’єкт-предикат-об’єкт\"):")
    for s, p, o in g:
        print(s, p, o)
    print(f"\n")

    print("4. Вивести список усіх англомовних елементів із моделі даних.")
    for *_, o in g:
        lang = langid.classify(o)[0]
        if lang == "en":
            print(o)
