import datetime
import lxml.html

from lxml import etree
from xml.etree import ElementTree


XML_FILE_NAME = "initial.xml"
NEW_XML_FILE_NAME = "new.xml"
XSLT_FILE_NAME = "template.xslt"
OUTPUT_HTML_FILE_NAME = "output.html"

FIELDS = {field: index for index, field in enumerate([
    'title', 'link', 'price', 'shipping', 'image_src',
    'offer', 'location', 'hotness',
])}


def update_xml():
    tree = ElementTree.parse(XML_FILE_NAME)
    root = tree.getroot()

    for index, _ in enumerate(root[:3:]):
        root[index][FIELDS['price']].text = str(14.88)
        root[index].remove(root[index][FIELDS['hotness']])
        ElementTree.SubElement(root[index], 'custom_field', {'type': 'datetime'}).text = str(datetime.datetime.now())
    tree.write(NEW_XML_FILE_NAME)


def generate_html():
    xslt_doc = etree.parse(XSLT_FILE_NAME)
    xslt_transformer = etree.XSLT(xslt_doc)

    source_doc = etree.parse(NEW_XML_FILE_NAME)
    output_doc = xslt_transformer(source_doc)

    output_doc.write(OUTPUT_HTML_FILE_NAME, pretty_print=True)


if __name__ == "__main__":
    update_xml()
    generate_html()
