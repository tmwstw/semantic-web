from rdflib import Graph


def queries(g):
    ##########################################################################
    query = """
        SELECT (COUNT(*) as ?pCount)
        WHERE
        {
            ?s ?p ?o .
        }
    """
    result = g.query(query)
    print(f"1. Вивести загальну кількість триплетів: {list(result)[0][0]}\n\n")

    ##########################################################################
    query = """
        PREFIX dc: <http://purl.org/dc/elements/1.1/>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        SELECT ?name
        WHERE {
            ?titleObject gd:title ?name .
            ?titleObject rdf:type ?titleType .
            ?titleType rdfs:comment "Describes good title"
        }
        LIMIT 1
    """

    result = g.query(query)
    print(f"2. Вивести назву одного ресурсу (dc:title):\n",
        [row for row in result][0][0], "\n\n")

    ##########################################################################
    query = """
        PREFIX dc: <http://purl.org/dc/elements/1.1/>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        SELECT DISTINCT ?info
        WHERE {
            ?s ?p ?info .
            ?s rdf:type ?t .
            ?t rdfs:domain ?domain .
            ?domain dc:title "goodInfo" .
        }
    """

    result = g.query(query)
    responce = "\n".join([str(row[0]) for row in result])
    print(f"3. Вивести список всіх ресурсів одного складного\
        класу:\n{responce}\n\n")

    ##########################################################################
    query = """
        PREFIX dc: <http://purl.org/dc/elements/1.1/>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        SELECT ?info
        WHERE {
            ?s gd:price ?info .
            ?s rdf:type ?t .
            ?t rdfs:domain ?domain .
            ?domain dc:title "goodInfo" .
        }
    """

    result = g.query(query)
    responce = "\n".join([str(row[0]) for row in result])
    print(f"4. Вивести інформацію за однією із характеристик для всіх \
        описаних ресурсів:\n{responce}\n\n")

    ##########################################################################
    query = """
        PREFIX dc: <http://purl.org/dc/elements/1.1/>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        SELECT ?name
        WHERE {
            ?s dc:title ?name .
            ?s rdfs:subClassOf ?x .
            ?x dc:title "listOfGoods"
        }
    """

    result = g.query(query)
    responce = "\n".join([str(row[0]) for row in result])
    print(f"5. Вивести назви всіх підкласів для базового класу \
        listOfGoods:\n{responce}\n\n")

    ##########################################################################
    query = """
        PREFIX dc: <http://purl.org/dc/elements/1.1/>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        SELECT ?p (COUNT(?p) as ?pc)
        WHERE {
            ?s ?p ?o .
        }
        GROUP BY ?p
    """

    result = g.query(query)
    print(f"6. Частота використання предікатів:\n")
    for predicate_, count_ in result:
        print(predicate_, count_)
    print("\n")

    ##########################################################################
    query = """
        PREFIX dc: <http://purl.org/dc/elements/1.1/>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        SELECT (COUNT(?sub_class_title) as ?pCount)
        WHERE
        {
            ?base dc:title "good" .
            ?sub_class rdfs:subClassOf ?base .
            ?sub_class dc:title ?sub_class_title .
        }
    """

    result = g.query(query)
    responce = "\n".join(str(row[0]) for row in result)
    print(f"7. Кількість підкласів класу good:\n")
    print(f"{responce}\n\n")

    ##########################################################################


if __name__ == "__main__":
    graph = Graph()
    graph.parse('file.rdf')

    queries(graph)
